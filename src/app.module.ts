import { join } from 'path';
import { Connection } from 'typeorm';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';

import { BookModule } from './controllers/book/book.module';
import { Book } from './controllers/book/book.entity';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'newuser',
      password: 'newuser',
      database: 'books',
      entities: [Book],
      synchronize: true,
    }),
    BookModule,
  ],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
