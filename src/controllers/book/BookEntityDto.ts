import {
  Max,
  Min,
  IsInt,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class BookEntityDto {
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  name: string;

  @IsInt()
  @Min(1)
  @Max(10e3)
  pages: number;
}
