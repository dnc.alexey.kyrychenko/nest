import {
  Get,
  Post,
  Put,
  Delete,
  Body,
  Query,
  Param,
  UsePipes,
  Controller,
  ParseIntPipe,
  InternalServerErrorException,
  ValidationPipe,
} from '@nestjs/common';
import { BookService } from './book.service';
import { BookEntityDto } from './BookEntityDto';

@Controller('api/book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get()
  async getBook(@Query('name') name: string) {
    let res = name
      ? await this.bookService.getBook(name)
      : await this.bookService.getAllBooks();
    if (res) {
      return {
        data: res,
      };
    }
    throw new InternalServerErrorException();
  }

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  async addNewBook(@Body() bookEntityDto: BookEntityDto) {
    const { name, pages } = bookEntityDto;
    return this.bookService.addBook(name, pages).then(
      () => ({ message: 'Success' }),
      () => {
        throw new InternalServerErrorException();
      },
    );
  }

  @Put('update/:id')
  updateBook(@Param('id', ParseIntPipe) id: number, @Body() body) {
    this.bookService
      .updateBook(id, body.name, Number.parseInt(body.pages))
      .then(
        () => ({
          message: 'Success',
        }),
        () => {
          throw new InternalServerErrorException();
        },
      );
  }

  @Delete('delete/:id')
  deleteBook(@Param('id', ParseIntPipe) id: number) {
    this.bookService.deleteBook(id).then(
      () => ({
        message: 'Success',
      }),
      () => {
        throw new InternalServerErrorException();
      },
    );
  }
}
