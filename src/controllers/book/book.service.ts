import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from './book.entity';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private readonly bookRepository: Repository<Book>,
  ) {}

  getAllBooks() {
    return this.bookRepository.find();
  }

  getBook(name: string) {
    return this.bookRepository.findOne({ name });
  }

  addBook(name: string, pages: number) {
    const book = this.bookRepository.create({ name, pages });
    return this.bookRepository.save(book);
  }

  updateBook(id: number, name: string, pages: number) {
    return this.bookRepository.update({ id }, { name, pages });
  }

  deleteBook(id: number) {
    return this.bookRepository.delete(id);
  }
}
