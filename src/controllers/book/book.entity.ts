import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('bookslist')
export class Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  pages: number;
}
